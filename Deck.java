/*
 * Deck.java
 *
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;
import java.util.Random;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Collections;
public class Deck
{
    //We use an array because we will possibly need to remove elements from it.
    private ArrayList<UnoCard> myDeck;

    /*
     * Although redundant, therefore consuming more memory, using a Map
     * provides to quick access if you know what you're looking for
     * but don't care about its position or ordering.
     * In this case, we will use it to link a card type (see Unocard.hashCode())
     * with the number of times it appears on a deck.
     */
    Map<UnoCard, Integer> deckCards; 

    public Deck(int size)
    {
        myDeck = new ArrayList<>(size);
    }

    public Deck(ArrayList<UnoCard> deck)
    {
        myDeck = deck;
    }

    public Deck() 
    {	
        //Predefining size is a quick optimization, so that it is not resized with each addition.
        myDeck = new ArrayList<>(108);

        //Uses Unocard.hashCode() internally
        deckCards = new HashMap<>();

        //Boring code. 
        myDeck.add(new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.ZERO));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.ONE));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.ONE));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.TWO));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.TWO));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.THREE));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.THREE));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.FOUR));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.FOUR));
        myDeck.add(  new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SKIP));
        myDeck.add( new UnoCard(UnoCard.Colors.RED,
                    UnoCard.Effects.SKIP));

        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.ZERO));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.ONE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.ONE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.FOUR));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.FOUR));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SKIP));
        myDeck.add( new UnoCard(UnoCard.Colors.BLUE,
                    UnoCard.Effects.SKIP));

        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.ZERO));    
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.ONE));      
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.ONE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.FOUR)); 
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.FOUR)); 
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SKIP));
        myDeck.add( new UnoCard(UnoCard.Colors.GREEN,
                    UnoCard.Effects.SKIP));

        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.ZERO)); 
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.ONE));  
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.ONE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.TWO));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.THREE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.FOUR));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.FOUR)); 
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.FIVE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SIX));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SEVEN));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.EIGHT));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.NINE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.REVERSE));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.TAKE2));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SKIP));
        myDeck.add( new UnoCard(UnoCard.Colors.YELLOW,
                    UnoCard.Effects.SKIP));

        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKER));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKER));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKER));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKER));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKERTAKE4));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKERTAKE4));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKERTAKE4));
        myDeck.add( new UnoCard(UnoCard.Colors.NONE,
                    UnoCard.Effects.JOKERTAKE4));

        /*
         * Here we count the cards of each type in the deck and
         * encode that information in our Map
         */
        for (UnoCard c : myDeck)
        {
            if (deckCards.containsKey(c))
            {
                deckCards.replace(c, deckCards.get(c)+1);
            }
            else 
                deckCards.put(c, 1);
        }
        /* System.out.println("Finish hash"); */

    }

    //Implements Knuth Shuffle, a simple but (kinda) unbiased shuffling procedure.
    public void shuffle()
    {
        Random rand = new Random();
        for (int i = 0; i < 108; i++) {
            int n = rand.nextInt(108); 
            UnoCard tmp = myDeck.get(n);
            myDeck.set(n, myDeck.get(i));
            myDeck.set(i, tmp);
        }
    }

    public UnoCard giveRandomCard()
    {
        Random rand = new Random();
        int num = rand.nextInt(108);
        return giveCardAt(num);
    }


    public UnoCard showRandomCard()
    {
        Random rand = new Random();
        int num = rand.nextInt(108);
        return showCardAt(num);
    }

    public UnoCard giveFirstCard()
    {
        return giveCardAt(0);
    }

    public UnoCard giveBackCard()
    {
        return giveCardAt(myDeck.size()-1);
    }

    public UnoCard showFirstCard()
    {
        return showCardAt(0);
    }

    public UnoCard showLast()
    {
        return showCardAt(myDeck.size()-1);
    }

    //Returns a Card and removes it from our Deck
    public UnoCard giveCardAt(int n)
    {
        UnoCard card = myDeck.get(n);
        myDeck.remove(n);
        deckCards.replace(card, deckCards.get(card)-1);
        return card;
    }

    //Returns a Card but it actually removes it from our Deck.
    public UnoCard showCardAt(int n)
    {
        UnoCard card = myDeck.get(n);
        return card;
    }

    public void giveSubDeck()
    {

    }

    public Deck showSubDeck(int low, int high)
    {
        Deck newDeck = new Deck(high-low);
        for (int i = low; i <= high; i++) {
            newDeck.insertCardLast(this.showCardAt(i));
        }
        return newDeck;
    }

    public void insertCardFirst(UnoCard card)
    {
        insertCard(card, 0);
    }

    public void insertCardLast(UnoCard card)
    {
        insertCard(card, myDeck.size()-1);
    }

    public void insertCardRandom(UnoCard card)
    {
        Random rand = new Random();
        insertCard(card, rand.nextInt(108));
    }

    /*
     * Thanks to our apparently redundant structure,
     * we can quickly determine if a card excedes its
     * maximmum occurences in a Deck.
     * So, we will throw an exception if that happens.
     */
    public void insertCard(UnoCard card, int n)
    {
        if((card.getMyEffect()==UnoCard.Effects.JOKER && deckCards.get(card)<4 )||
                (card.getMyEffect()==UnoCard.Effects.JOKERTAKE4 && deckCards.get(card) <4 ) ||
                (card.getMyEffect()==UnoCard.Effects.ZERO && deckCards.get(card) <1) ||
                (deckCards.get(card) <2))
        {
            myDeck.add(n, card);
            deckCards.replace(card, deckCards.get(card)+1);
        }
        else
        {
            throw new IllegalArgumentException("Card already exists");
        }

    }

    public void requestCard()
    {

    }

    public void requestCardPosition()
    {

    }

    public boolean isSorted()
    {
        for (int i = 1; i < myDeck.size(); i++) {
            if (myDeck.get(i).compareTo(myDeck.get(i-1)) < 0)
                return false;
        }
        return true;
    }

    public int size()
    {
        return myDeck.size();
    }

    public void swap(int a, int b)
    {
        UnoCard card1 = myDeck.get(a);
        myDeck.set(a, myDeck.get(b));
        myDeck.set(b, card1);
    }

    public void populateMap()
    {
        deckCards.clear();
        for (UnoCard c : myDeck)
        {
            if (deckCards.containsKey(c))
            {
                deckCards.replace(c, deckCards.get(c)+1);
            }
            else 
                deckCards.put(c, 1);
        }
    }

    public boolean isValid()
    {
        System.out.println(deckCards.keySet());
        for (UnoCard card : deckCards.keySet() ) 
        {
            if (deckCards.get(card) > cardLimit(card) )
                return false;
        }
        return true;
    }

    public int cardLimit(UnoCard card)
    {
        if (card.getMyEffect()==UnoCard.Effects.JOKER  || card.getMyEffect()==UnoCard.Effects.JOKERTAKE4)
            return 4;
        else if(card.getMyEffect()==UnoCard.Effects.ZERO)
            return 1;
        else return 2;
    }

    //TODO
    public static boolean isValid(Deck deck)
    {
        return false;
    }



    @Override
    public String toString()
    {
        StringBuilder tmp = new StringBuilder();
        for (UnoCard c : myDeck ) {
            tmp.append(c);
            tmp.append("\n");
        }
        return tmp.toString();
    }
}
