/*
 * UnoCard.java
 *
 */

/* Describes an abstract UNO card, uses the following int codes:
 * 0=0, 1=1, 2=2,...9=9
 * 10 = block turn 
 * 11 = reverse
 * 12 = take 2
 * 13 = Joker (color -1)
 * 14 = Joker + take 4 (color -1)
 * 
 * Rules:
 * First int color, second int number or effect
 * If card is joker, color is set to -1;
 */
import java.util.Objects;
public class UnoCard implements Comparable<UnoCard>
{
    /*
     * Enum types are basically Java's type safe constants, they restrict parameters to a given set.
     */
    public enum Colors 
    {
        RED,
        BLUE,
        GREEN,
        YELLOW,
        NONE
    }

    /*
     * The natural order of enums is automatically set as the order in which they are declared (ONE before TWO)
     */
    public enum Effects
    {
        ZERO,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        REVERSE,
        TAKE2,
        SKIP,
        JOKER,
        JOKERTAKE4
    }

    /*
     * Made final so that it cannot be changed after creation.
     */
    private final Colors myColor;
    private final Effects myEffect;

    /*
     * Needs to check that color is in the adequate range
     * Needs to check that if card is joker, then it has no color and vicevers
     * Throws IllegalArgumentException if not validated 
     */
    public UnoCard(Colors color, Effects effect)
    {
        if (color == Colors.NONE && !(effect != Effects.JOKER || effect != Effects.JOKERTAKE4)) throw new IllegalArgumentException("Bad Argument: " +color + " "+effect + 
                " "+ (color == Colors.NONE));
        if (color != Colors.NONE && (effect == Effects.JOKER || effect == Effects.JOKERTAKE4)) throw new IllegalArgumentException("Bad Argument:" +color +" " + effect);
        myColor = color;
        myEffect = effect;
    }

    /*
     * This will uniquely identify each card type, so that a green #2 has a different code that green #3 or red #3 but the same as another green #2
     */
    @Override 
    public int hashCode()
    {
        return Objects.hash(myColor, myEffect);
    }

    @Override
    public String toString()
    {
        return "Color:" + this.myColor + " Effect: " + this.myEffect ;
    }

    public Colors getMyColor() {
        return myColor;
    }

    public Effects getMyEffect() {
        return myEffect;
    }

    public boolean isJoker()
    {
        if (myEffect == Effects.JOKER || myEffect == Effects.JOKERTAKE4) return true;
        else return false;
    }




    @Override
    public int compareTo(UnoCard other)
    {
        if(myColor == other.getMyColor())
        {
            return myEffect.compareTo(other.getMyEffect());
        }
        else
        {
            return myColor.compareTo(other.getMyColor());
        }
    }

    @Override
    public boolean equals(Object other)
    {
        if(this == other)
            return true;
        if(other == null)
            return false;
        if(this.getClass() != other.getClass())
            return false;
        UnoCard otherCard = (UnoCard) other;
        return (otherCard.getMyEffect() == this.myEffect && otherCard.getMyColor() == this.myColor);
    }
}
